﻿namespace tictactoe
{
    partial class IksOks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ponovnoBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.prviImeBox = new System.Windows.Forms.TextBox();
            this.drugiImeBox = new System.Windows.Forms.TextBox();
            this.igrajBtn = new System.Windows.Forms.Button();
            this.imePrviTxt = new System.Windows.Forms.Label();
            this.prviScoreTxt = new System.Windows.Forms.Label();
            this.imeDrugiTxt = new System.Windows.Forms.Label();
            this.drugiScoreTxt = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.potezImeTxt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "1";
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(139, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(120, 120);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "2";
            this.pictureBox2.Visible = false;
            this.pictureBox2.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.InitialImage = null;
            this.pictureBox3.Location = new System.Drawing.Point(265, 13);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(120, 120);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "3";
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.InitialImage = null;
            this.pictureBox4.Location = new System.Drawing.Point(13, 139);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(120, 120);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "4";
            this.pictureBox4.Visible = false;
            this.pictureBox4.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.InitialImage = null;
            this.pictureBox5.Location = new System.Drawing.Point(139, 139);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(120, 120);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "5";
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.InitialImage = null;
            this.pictureBox6.Location = new System.Drawing.Point(265, 139);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(120, 120);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "6";
            this.pictureBox6.Visible = false;
            this.pictureBox6.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.InitialImage = null;
            this.pictureBox7.Location = new System.Drawing.Point(13, 265);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(120, 120);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "7";
            this.pictureBox7.Visible = false;
            this.pictureBox7.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.InitialImage = null;
            this.pictureBox8.Location = new System.Drawing.Point(139, 265);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(120, 120);
            this.pictureBox8.TabIndex = 7;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "8";
            this.pictureBox8.Visible = false;
            this.pictureBox8.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = global::tictactoe.Properties.Resources.BLANK;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.InitialImage = null;
            this.pictureBox9.Location = new System.Drawing.Point(265, 265);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(120, 120);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "9";
            this.pictureBox9.Visible = false;
            this.pictureBox9.Click += new System.EventHandler(this.funckijaKlik);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(310, 490);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Izlaz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ponovnoBtn
            // 
            this.ponovnoBtn.Location = new System.Drawing.Point(184, 445);
            this.ponovnoBtn.Name = "ponovnoBtn";
            this.ponovnoBtn.Size = new System.Drawing.Size(75, 23);
            this.ponovnoBtn.TabIndex = 10;
            this.ponovnoBtn.Text = "Predaj se";
            this.ponovnoBtn.UseVisualStyleBackColor = true;
            this.ponovnoBtn.Visible = false;
            this.ponovnoBtn.Click += new System.EventHandler(this.ponovnoBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 395);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Ime X igrača:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 422);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ime O igrača:";
            // 
            // prviImeBox
            // 
            this.prviImeBox.Location = new System.Drawing.Point(139, 392);
            this.prviImeBox.MaxLength = 10;
            this.prviImeBox.Name = "prviImeBox";
            this.prviImeBox.Size = new System.Drawing.Size(120, 20);
            this.prviImeBox.TabIndex = 13;
            // 
            // drugiImeBox
            // 
            this.drugiImeBox.Location = new System.Drawing.Point(139, 419);
            this.drugiImeBox.MaxLength = 10;
            this.drugiImeBox.Name = "drugiImeBox";
            this.drugiImeBox.Size = new System.Drawing.Size(120, 20);
            this.drugiImeBox.TabIndex = 14;
            // 
            // igrajBtn
            // 
            this.igrajBtn.Location = new System.Drawing.Point(310, 438);
            this.igrajBtn.Name = "igrajBtn";
            this.igrajBtn.Size = new System.Drawing.Size(75, 23);
            this.igrajBtn.TabIndex = 15;
            this.igrajBtn.Text = "Igraj";
            this.igrajBtn.UseVisualStyleBackColor = true;
            this.igrajBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // imePrviTxt
            // 
            this.imePrviTxt.AutoSize = true;
            this.imePrviTxt.Location = new System.Drawing.Point(136, 395);
            this.imePrviTxt.Name = "imePrviTxt";
            this.imePrviTxt.Size = new System.Drawing.Size(23, 13);
            this.imePrviTxt.TabIndex = 16;
            this.imePrviTxt.Text = "ime";
            this.imePrviTxt.Visible = false;
            // 
            // prviScoreTxt
            // 
            this.prviScoreTxt.AutoSize = true;
            this.prviScoreTxt.Location = new System.Drawing.Point(314, 396);
            this.prviScoreTxt.Name = "prviScoreTxt";
            this.prviScoreTxt.Size = new System.Drawing.Size(13, 13);
            this.prviScoreTxt.TabIndex = 17;
            this.prviScoreTxt.Text = "0";
            this.prviScoreTxt.Visible = false;
            // 
            // imeDrugiTxt
            // 
            this.imeDrugiTxt.AutoSize = true;
            this.imeDrugiTxt.Location = new System.Drawing.Point(136, 422);
            this.imeDrugiTxt.Name = "imeDrugiTxt";
            this.imeDrugiTxt.Size = new System.Drawing.Size(23, 13);
            this.imeDrugiTxt.TabIndex = 18;
            this.imeDrugiTxt.Text = "ime";
            this.imeDrugiTxt.Visible = false;
            // 
            // drugiScoreTxt
            // 
            this.drugiScoreTxt.AutoSize = true;
            this.drugiScoreTxt.Location = new System.Drawing.Point(314, 422);
            this.drugiScoreTxt.Name = "drugiScoreTxt";
            this.drugiScoreTxt.Size = new System.Drawing.Size(13, 13);
            this.drugiScoreTxt.TabIndex = 19;
            this.drugiScoreTxt.Text = "0";
            this.drugiScoreTxt.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(265, 395);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Bodovi:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(265, 422);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Bodovi:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 450);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Na potezu je:";
            // 
            // potezImeTxt
            // 
            this.potezImeTxt.AutoSize = true;
            this.potezImeTxt.Location = new System.Drawing.Point(89, 450);
            this.potezImeTxt.Name = "potezImeTxt";
            this.potezImeTxt.Size = new System.Drawing.Size(16, 13);
            this.potezImeTxt.TabIndex = 23;
            this.potezImeTxt.Text = "...";
            // 
            // IksOks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 526);
            this.Controls.Add(this.potezImeTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.drugiScoreTxt);
            this.Controls.Add(this.imeDrugiTxt);
            this.Controls.Add(this.prviScoreTxt);
            this.Controls.Add(this.imePrviTxt);
            this.Controls.Add(this.igrajBtn);
            this.Controls.Add(this.drugiImeBox);
            this.Controls.Add(this.prviImeBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ponovnoBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "IksOks";
            this.Text = "X-O";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ponovnoBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox prviImeBox;
        private System.Windows.Forms.TextBox drugiImeBox;
        private System.Windows.Forms.Button igrajBtn;
        private System.Windows.Forms.Label imePrviTxt;
        private System.Windows.Forms.Label prviScoreTxt;
        private System.Windows.Forms.Label imeDrugiTxt;
        private System.Windows.Forms.Label drugiScoreTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label potezImeTxt;
    }
}

