﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe
{
    public partial class IksOks : Form
    {
        int[] kliknuto = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int igrac = 1;  
        bool pobjeda = false;
        int iks = 0, oks = 0;  

        public IksOks()
        {
            InitializeComponent();
        }

       
        private void button3_Click(object sender, EventArgs e)
        {
            if (prviImeBox.Text == "" || drugiImeBox.Text == "")
                MessageBox.Show("Neko od polja ostalo je prazno.", "Pogreška!");
            else
            {
               
                imePrviTxt.Text = prviImeBox.Text;
                imeDrugiTxt.Text = drugiImeBox.Text;
                igrajBtn.Visible = false;
                prviImeBox.Visible = false;
                drugiImeBox.Visible = false;
                pictureBox1.Visible=true;
                pictureBox2.Visible = true;
                pictureBox3.Visible = true;
                pictureBox4.Visible = true;
                pictureBox5.Visible = true;
                pictureBox6.Visible = true;
                pictureBox7.Visible = true;
                pictureBox8.Visible = true;
                pictureBox9.Visible = true;
                imePrviTxt.Visible = true;
                imeDrugiTxt.Visible = true;
                ponovnoBtn.Visible = true;
                prviScoreTxt.Visible = true;
                drugiScoreTxt.Visible = true;
            }

            potezImeTxt.Text = imePrviTxt.Text;
        }

    
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        public void funckijaKlik(object sender, EventArgs e) 
        {
            string test = (string)((PictureBox)sender).Tag;
            int tagBroj = Int32.Parse(test); 

            if(kliknuto[tagBroj-1]==0 && !pobjeda){
                if(igrac%2==1) { 
                    ((PictureBox)sender).BackgroundImage = tictactoe.Properties.Resources.iks;
                    kliknuto[tagBroj-1]=1;
                    igrac += 1; 
                    potezImeTxt.Text = imeDrugiTxt.Text;
                }
                else{ 
                    ((PictureBox)sender).BackgroundImage = tictactoe.Properties.Resources.oks; 
                    kliknuto[tagBroj - 1] = 2; 
                    igrac += 1; 
                    potezImeTxt.Text = imePrviTxt.Text; 
                }

            }else if(!pobjeda){
                 MessageBox.Show("Već kliknuto!", "Pogreška!");
            }else
                MessageBox.Show("Igra je završila!", "Pogreška!");
          
           if(igrac>=4)
                provjera();
            
        }

        
        public void provjera()
        {
            if (igrac % 2 == 0 && !pobjeda)
            { 
                if (kliknuto[0] == 1 && kliknuto[1] == 1 && kliknuto[2] == 1)
                    iks_pobjeduje();
                else if (kliknuto[3] == 1 && kliknuto[4] == 1 && kliknuto[5] == 1)
                    iks_pobjeduje();
                else if (kliknuto[6] == 1 && kliknuto[7] == 1 && kliknuto[8] == 1)
                    iks_pobjeduje();
                else if (kliknuto[0] == 1 && kliknuto[4] == 1 && kliknuto[8] == 1)
                    iks_pobjeduje();
                else if (kliknuto[2] == 1 && kliknuto[4] == 1 && kliknuto[6] == 1)
                    iks_pobjeduje();
                else if (kliknuto[2] == 1 && kliknuto[4] == 1 && kliknuto[6] == 1)
                    iks_pobjeduje();
                else if (kliknuto[0] == 1 && kliknuto[3] == 1 && kliknuto[6] == 1)
                    iks_pobjeduje();
                else if (kliknuto[1] == 1 && kliknuto[4] == 1 && kliknuto[7] == 1)
                    iks_pobjeduje();
                else if (kliknuto[2] == 1 && kliknuto[5] == 1 && kliknuto[8] == 1)
                    iks_pobjeduje();
            }
            else if (!pobjeda)
            {  
                if (kliknuto[0] == 2 && kliknuto[1] == 2 && kliknuto[2] == 2)
                    oks_pobjeduje();
                else if (kliknuto[3] == 2 && kliknuto[4] == 2 && kliknuto[5] == 2)
                    oks_pobjeduje();
                else if (kliknuto[6] == 2 && kliknuto[7] == 2 && kliknuto[8] == 2)
                    oks_pobjeduje();
                else if (kliknuto[0] == 2 && kliknuto[4] == 2 && kliknuto[8] == 2)
                    oks_pobjeduje();
                else if (kliknuto[2] == 2 && kliknuto[4] == 2 && kliknuto[6] == 2)
                    oks_pobjeduje();
                else if (kliknuto[2] == 2 && kliknuto[4] == 2 && kliknuto[6] == 2)
                    oks_pobjeduje();
                else if (kliknuto[0] == 2 && kliknuto[3] == 2 && kliknuto[6] == 2)
                    oks_pobjeduje();
                else if (kliknuto[1] == 2 && kliknuto[4] == 2 && kliknuto[7] == 2)
                    oks_pobjeduje();
                else if (kliknuto[2] == 2 && kliknuto[5] == 2 && kliknuto[8] == 2)
                    oks_pobjeduje();
            }
            if (igrac == 10 && !pobjeda)
            {
                MessageBox.Show("Neriješeno!", "Info");
                ponovno();
            }
        }

        public void iks_pobjeduje()
        {
            MessageBox.Show(imePrviTxt.Text + " je pobijedio!", "Pobjeda!");
            pobjeda = true;
            iks += 1;
            prviScoreTxt.Text = iks.ToString();
            ponovno();
        }

        public void oks_pobjeduje()
        {
            MessageBox.Show(imeDrugiTxt.Text + " je pobijedio!", "Pobjeda!");
            pobjeda = true;
            oks += 1;
            drugiScoreTxt.Text = oks.ToString();
            ponovno();
        }

        
        public void ponovno()
        {
            
            kliknuto = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            igrac = 1;
            pobjeda = false;
            
            pictureBox1.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox2.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox3.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox4.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox5.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox6.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox7.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox8.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            pictureBox9.BackgroundImage = tictactoe.Properties.Resources.BLANK;
            potezImeTxt.Text = imePrviTxt.Text; 
        }

    

        
        private void ponovnoBtn_Click(object sender, EventArgs e)
        {
            if (igrac % 2 == 1) 
            {
                MessageBox.Show(imePrviTxt.Text + " se je predao!", "Predaja!");
                iks -= 1;
                prviScoreTxt.Text = iks.ToString();
                ponovno();
            }
            else
            {
                MessageBox.Show(imeDrugiTxt.Text + " se je predao!", "Predaja!");
                oks -= 1;
                drugiScoreTxt.Text = oks.ToString();
                ponovno();
            }
        }



    }
}
